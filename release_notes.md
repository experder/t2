## Release notes

[[Coming features](https://github.com/experder/T2/blob/master/dev/notes.md#current-todos)]

### v0.2
**Release date:** 2020-01-29  
**DB version:** 4  
**Release notes:**  
Work in progress.
Classes: Error_, Stylesheet, Login, Ajax, new Formfields (checkbox, header, radio, textarea), Includes (jQuery, Parsedown), Warning, Autoloader.
Services: DB initialization, Filewriter, get_api_class, Arrays, Module Generator.
Developers: $DEVMODE, Module template, TODOs.
Basic CSS, core_prefix prepared, errorhandling, prompt and init config_params, API: default_values, module configuration, config defaults, basic js, init platform.
Enhanced: Updater (Windows/Linux), Installer, dev-stats (queries, core queries, runtime, memory, includes, core includes), namespaces, Config: cache stored values, Page, Installation wizard, documentation (regex, submodules), Form and Formfields, config.
Fixed: user id not available on login form.  
**Extinctions:** Old Error class, dev-tools (extra repository), class service\Html (included in core\Html)

### v0.1
**Release date:** 2019-12-29  
**DB version:** -  
**Release notes:**  
New readme, some links,
Classes: Page, Error, Message, Forms, Database, Html.
Service classes: Config, Files, Html, Request, Strings, Templates.
Compiler messages, Installer, Updater, dev stats.
PHP language level: 5.3.  
**Extinctions:** None

