# T2
3rd shot of a great PHP toolbox.

Inspired by: [Tethys-Beta](https://github.com/gitfabian/tethysbeta) (2014) and [Tethys](https://github.com/GitFabian/Tethys) (2018).

[[Content](https://github.com/experder/T2/blob/master/help/folders.md)]
[[Installation](https://github.com/experder/T2/blob/master/help/install.md)]

### More

[[Useful links](https://github.com/experder/T2/blob/master/help/links.md)]
[[Developers](https://github.com/experder/T2/blob/master/dev/notes.md)]
[[Release notes](https://github.com/experder/T2/blob/master/release_notes.md)]
